import request from './request.js';
var dataService = {

    // function to add FireBase Token
    addFireBase(platform, uuid, tokenFireBase) {
        let url = 'apiShop/registerShop'
        let data = {
            platform: platform,
            uuid: uuid,
            registerId: tokenFireBase
        }
        return request.post(url, data)
    },
    // function to used code
    checkCode(code, value) {
        let url = 'apiShop/checkCodeByShop'
        let data = {
            code: code,
            cost: value
        }
        return request.post(url, data)
    },
    // function get list code not checked
    getlistCodeNotCheck(start, number, type, search) {
        let url = 'apiShop/listNotChecked'
        let data = {
            start: start,
            number: number,
            type: type,
            search: search
        }
        return request.post(url, data)
    },
    getlistCodeChecked(start, number, type, search) {
        let url = 'apiShop/listChecked'
        let data = {
            start: start,
            number: number,
            type: type,
            search: search
        }
        return request.post(url, data)
    },
    // function get his mgreen
    getHisMgreen(start, number, type, searchText) {
        let url = 'apiShop/historyMgreen'
        let data = {
            start: start,
            number: number,
            type: type,
            search: searchText
        }
        return request.post(url, data)
    },
    // submit mgreen
    submitMgreen(userId, type, taskId) {
        let url = 'apiShop/submitMgreen'
        let data = {
            endUserId: userId,
            taskId: taskId,
            type: type
        }
        return request.post(url, data)
    },
    getInfoShop() {
        let url = 'apiShop/getInfoShop'
        let data = {

        }
        return request.post(url, data)
    },
    updateUser(gender, name, address, birthday, email, phone) {
        let url = 'apiShop/updateUser'
        let data = {
            gender,
            name,
            address,
            birthday,
            email,
            phone
        }
        return request.post(url, data)
    },
    forgetPassword(userLogin) {
        let url = 'apiShop/authenShop/forgetPassword'
        let data = {
            userLogin: userLogin
        }
        return request.post(url, data)
    },
    uploadAvatar() {
        let url = 'apiShop/uploadAvatar'
        let data = {

        }
        return request.post(url, data)
    },
    verifyOtp(id, otp, newPassword) {
        let url = 'apiShop/authenShop/verifyOtp'
        let data = {
            id: id,
            otp: otp,
            newPassword: newPassword
        }
        return request.post(url, data)
    },
    changePassword(newPassword, password) {
        let url = 'apiShop/changePassword'
        let data = {
            newPassword: newPassword,
            password: password
        }
        return request.post(url, data)
    },


    ////////
    loginApp(username, password) {
        let url = 'user/login'
        let data = {
            username,
            password
        }
        return request.post(url, data)
    },

    shopGetHistories(skip, limit) {
        let url = 'code/shopGetHistories?page=57&api=shopGetHistories'
        let data = {
            limit,
            skip,

        }
        return request.post(url, data)
    },
    getPromotionByCardCode(code) {
        let url = 'shop/getPromotionByCardCode?page=57&api=getPromotionByCardCode'
        let data = {
            code
        }
        return request.post(url, data)
    },
    checkoutCardCode(code, promotion) {
        let url = 'shop/checkoutCardCode?page=57&api=checkoutCardCode'
        let data = {
            code, promotion
        }
        return request.post(url, data)
    },
    shopCheckoutCode(code, billAmount, receipt, receiptImage) {
        let url = 'code/shopCheckoutCode?page=57&api=shopCheckoutCode'
        let data = {
            code,
            billAmount,
            receipt,
            receiptImage

        }
        return request.post(url, data)
    },

    shopCheckoutPhone(phone, bill, receipt) {
        let url = 'code/addPointByPhone?page=57&api=addPointByPhone'
        let data = {
            phone, bill, receipt
        }
        return request.post(url, data)
    },

    shopGetHistoryInfo(id) {
        let url = 'code/shopGetHistoryInfo?page=57&api=shopGetHistoryInfo'
        let data = {
            id
        }
        return request.post(url, data)
    },

    refreshToken(token) {
        let url = 'user/refreshToken'
        let data = {
        }
        return request.post(url, data, token)
    },
    createOtp: (phone) => {
        var url = 'otp/createOTP';
        var data = {
            phone
        }
        return request.post(url, data);
    },
    confirmOTP: (id, otp) => {
        var url = 'otp/confirmOTP';
        var data = {
            id,
            otp
        }
        return request.post(url, data);
    },
    mergeShopUser: (otpToken) => {
        var url = 'user/mergeShopUser?page=37&api=mergeShopUser';
        var data = {
            otpToken
        }
        return request.post(url, data);
    },
    userChangePassword: (oldPassword, newPassword) => {
        var url = 'user/changeUserPassword?page=15&api=userChangePassword';
        var data = {
            oldPassword,
            newPassword
        }
        return request.post(url, data);
    },

    checkoutOrderCode: (code) => {
        var url = 'partnerproductorder/checkoutOrderCode?page=37&api=checkoutOrderCode';
        var data = {
            code
        }
        return request.post(url, data);
    },
    doneOrder: (id, code) => {
        var url = 'partnerproductorder/doneOrder?page=37&api=doneOrder';
        var data = {
            id,
            code
        }
        return request.post(url, data);
    },






}
export default dataService;