let initialState = {
    token: "",
    user: {

    },
    memberCard: {},
    memberInfo: {},
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER_INFO':
            return Object.assign({}, state, {
                user: action.user
            })
        case 'SET_TOKEN':
            return Object.assign({}, state, {
                token: action.token
            })
        case 'SET_MEMBER_INFO':
            return Object.assign({}, state, {
                memberInfo: action.memberInfo
            })
        case 'SET_MEMBER_CARD':
            return Object.assign({}, state, {
                memberCard: action.memberCard
            })
        case 'CLEAR_STORE': return initialState
        default:
            return state
    }
}

export default userReducer;