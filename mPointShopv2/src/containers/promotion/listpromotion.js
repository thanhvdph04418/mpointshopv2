
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image, Text,
    TouchableOpacity,
    ActivityIndicator,
    Platform
} from 'react-native';
import moment from 'moment'
import { Container, Icon, Header, Left, Body, Right, Title, Button, Input } from 'native-base'
import { Color } from '../../themes/color'
import dataService from '../../network/dataService'
import api from '../../api'
export default class ListPromotion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            refreshing: false
        }
    }
    componentDidMount() {
        this.getData()

    }
    isLoading = false
    async getData() {
        this.setState({
            refreshing: true,
            data: []
        })
        let { params } = this.props.navigation.state
        let rs = await dataService.getPromotionByCardCode(params.idCard);
        if (rs.code != 0) {
            this.setState({ refreshing: false })
            return api.showMessage(rs.msg)
        }
        this.setState({
            data: rs.data,
            refreshing: false,
        })
    }
    async checkoutCardCode(item) {
        let { params } = this.props.navigation.state
        api.showLoading()
        let rs = await dataService.checkoutCardCode(params.idCard, item.id);
        if (rs.code != 0) {
            api.hideLoading();
            return api.showMessage(rs.msg)
        }
        if (item.isBillPoint) {
            api.hideLoading();
            this.props.navigation.navigate('comfirmCode', { code: rs.data.code })
        } else {
            let comfirm = await dataService.shopCheckoutCode(rs.data.code);
            api.hideLoading();
            return api.showMessage(comfirm.msg)
        }
    }
    render() {
        return (
            <Container style={{ backgroundColor: '#E0E0E0' }}>
                <Header
                    androidStatusBarColor={Color.colorStatusbar}
                    style={{ backgroundColor: Color.colorPrimari }}>
                    <Left style={{ flex: 1 }}>
                        <Button onPress={() => { this.props.navigation.pop() }} transparent>
                            <Icon style={{ color: '#fff' }} name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 4 }}>
                        <Title style={{ color: 'white' }}>Danh sách ưu đãi</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>
                <FlatList
                    extraData={this.state}
                    data={this.state.data}
                    refreshing={Platform.OS == 'ios' ? false : this.state.refreshing}
                    onRefresh={this.getData.bind(this)}
                    ListEmptyComponent={
                        !this.state.refreshing ? <Text style={{ fontWeight: '500', marginTop: 30, textAlign: 'center' }}>Không có dữ liệu!</Text> : null
                    }
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity
                                activeOpacity={0.5}
                                style={styles.wrapItem}
                                onPress={() => { this.checkoutCardCode(item) }}
                            >
                                <Text style={styles.name}>{item.name}</Text>
                                {(item.isPercent && item.percent) ?
                                    <Text style={styles.txtTag}> - Giảm {item.percent}%</Text> : null
                                }
                                {item.isForSale ?
                                    <View>
                                        <Text style={styles.txtTag}>
                                            - Giảm còn: {item.salePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                        </Text>
                                        <Text style={{ fontSize: 12, textDecorationLine: 'line-through', fontStyle: 'italic' }}>
                                            Giá gốc: {item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                        </Text>
                                    </View> : null
                                }
                                {item.isGiftPoint ?
                                    <Text style={styles.txtTag}>- Tặng : {item.giftPoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text> : null
                                }
                                {item.isExchangePoint ?
                                    <View>
                                        <Text style={styles.txtTag}>- Trị giá : {item.exchangePoint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
                                        {item.price ? <Text style={{ fontSize: 12, textDecorationLine: 'line-through', fontStyle: 'italic' }}>
                                            Giá gốc: {item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                        </Text> : null}
                                    </View>
                                    : null
                                }
                                {item.isBillPoint ?
                                    <Text style={styles.txtTag}>- Tích: {item.billPointPercent}% trên tổng hóa đơn</Text> : null
                                }
                                <Image source={{ uri: item.images[0] }} style={styles.image} />
                                <Text style={{ marginLeft: 7, marginRight: 7, marginBottom: 0, fontWeight: '500', marginTop: 5 }}>Chi tiết ưu đãi </Text>
                                <Text style={{ marginLeft: 9, marginTop: 2 }}>{item.description}</Text>
                                <Text style={{ fontStyle: 'italic', textAlign: 'right', marginRight: 7, marginBottom: 7, color: '#e57373' }}>Ngày hết hạn: {moment(item.endDate).format('DD/MM/YYYY')}</Text>

                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item, index) => 'index' + index}
                />
            </Container>
        );
    }
}
const styles = StyleSheet.create({
    wrapItem: {
        marginTop: 10,
        borderRadius: 8,
        overflow: 'hidden',
        backgroundColor: 'white',
        width: api.getRealDeviceWidth() * 0.97,
        alignSelf: 'center'
    },
    image: {
        width: api.getRealDeviceWidth() * 0.97,
        height: (api.getRealDeviceWidth() * 0.97) / 2,
        resizeMode: 'stretch',
        marginTop: 5
    },
    name: {
        fontWeight: '500',
        marginLeft: 7,
        marginRight: 7,
        marginTop: 7

    },
    txtTag: {
        color: Color.colorPrimari,
        fontWeight: '500',
        marginLeft: 7,
        marginRight: 7,
    }

})