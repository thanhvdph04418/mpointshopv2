import React, { Component } from 'react';
import {
    View, Text, ImageBackground, TextInput, StyleSheet,
    TouchableOpacity, Alert, Image, Platform, ScrollView
} from 'react-native';
import { Container, Icon, Header, Left, Body, Right, Title, Button } from 'native-base'
import dataService from '../../network/dataService'
import api from '../../api'
import { Color } from '../../themes/color'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import config from '../../config'
import { connect } from 'react-redux'
import Dash from 'react-native-dash';
var options = {
    title: 'Chọn hoá đơn',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};
class ComfirmCode extends Component {
    constructor(props) {
        super(props);
        let { params } = this.props.navigation.state
        this.state = {
            code: params ? params.code : '',
            totalBill: '',
            codeBill: '',
            imgAmount: { id: 0, img: '' },
        }
    }
    async  comfirm() {
        let { code, totalBill, codeBill, imgAmount } = this.state;
        if (!code) return api.showMessage('Vui lòng điền Mã ưu đãi')
        let cost = totalBill.replace(/\./g, '')
        if (/[^\d]/.test(cost)) return api.showMessage('Tổng hoá đơn không hợp lệ', 'Thông báo')
        api.showLoading()
        let rs = await dataService.shopCheckoutCode(code, totalBill, codeBill, imgAmount.id ? imgAmount.img : '')
        api.hideLoading();
        if (rs.code != 0) return api.showMessage(rs.msg)
        api.showMessage(rs.msg)
        this.setState({
            code: '',
            totalBill: '',
            codeBill: '',
            imgAmount: { id: 0, img: '' }
        })
    }
    setCode(code, kind) {
        console.log('type', kind);
        if (kind == 'code') return this.setState({ code })
        if (kind == 'codeBill') return this.setState({ codeBill: code })

    }
    _selectLibrary() {
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('kkkkkkkk', response)
            if (Platform.OS == "ios") {
                if (response && response.uri) {
                    this._up(response.uri.replace('file:///', ''))
                }
            } else {
                if (response && response.path) {
                    this._up(response.path)
                }
            }
        });
    }
    _openCam() {
        ImagePicker.launchCamera(options, (response) => {
            if (Platform.OS == "ios") {
                if (response && response.uri) {
                    this._up(response.uri.replace('file:///', ''))
                }
            } else {
                if (response && response.path) {
                    this._up(response.path)
                }
            }
        });
    }
    _up(uri) {
        RNFetchBlob.fetch('POST', config.HOST + 'image/upload?folder=profile', {
            Authorization: 'Bearer ' + this.props.token
        }, [
                {
                    name: 'file',
                    filename: 'file.jpg',
                    type: 'image/jpg',
                    data: RNFetchBlob.wrap(uri)
                }
            ]).then((res) => {
                console.log('thanhvd', res)
                res = JSON.parse(res.data)
                this.setState({ imgAmount: { id: 1, img: res.url } })
            }).catch((err) => {
                api.showMessage('lỗi up load ảnh', err);
            })

    }
    render() {
        return (
            <ImageBackground style={styles.bg} source={require('../../img/bg.png')}>
                <Header
                    androidStatusBarColor={Color.colorStatusbar}
                    style={{ backgroundColor: Color.colorPrimari }}>
                    <Left style={{ flex: 1 }}>
                        <Button onPress={() => { this.props.navigation.pop() }} transparent>
                            <Icon style={{ fontSize: 25, color: '#fff' }} name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 4 }}>
                        <Title style={{ color: '#fff' }}>Xác thực mã ưu đãi</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ backgroundColor: 'white', width: '95%', alignSelf: 'center', marginTop: 10, overflow: 'hidden' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={styles.wrapInput}>
                                <Icon type='MaterialIcons' name='edit' style={styles.icon} />
                                <TextInput
                                    value={this.state.code}
                                    style={styles.input}
                                    placeholder='Nhập mã ưu đãi'
                                    onChangeText={(t) => { this.setState({ code: t }) }}
                                />
                            </View>
                            <Text style={styles.txt}>Hoặc</Text>
                            <TouchableOpacity
                                style={styles.bt}
                                onPress={() => {
                                    this.props.navigation.navigate('scanCode', {
                                        title: 'QUYÉT MÃ ƯU ĐÃI',
                                        setCode: this.setCode.bind(this),
                                        type: 'scanCode',
                                        kind: 'code'
                                    })
                                }}
                            >
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>Quét Mã</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={styles.wrapInput}>
                                <Icon type='FontAwesome' name='dollar' style={[styles.icon, { flex: 1.2 }]} />
                                <TextInput
                                    value={this.state.totalBill.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                    style={styles.input}
                                    placeholder='Tổng hóa đơn (VNĐ)(nếu có)'
                                    onChangeText={(text) => {
                                        let value = '';
                                        if (!value) value = text.replace(new RegExp('\\.', 'g'), '');
                                        this.setState({ totalBill: value })
                                    }
                                    }
                                    keyboardType='phone-pad'
                                />
                            </View>
                        </View>
                        {/* <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={styles.wrapInput}>
                                <Icon type='MaterialCommunityIcons' name='qrcode-scan' style={styles.icon} />
                                <TextInput
                                    value={this.state.codeBill}
                                    style={styles.input}
                                    placeholder='Mã hóa đơn (nếu có)'
                                    onChangeText={(text) => this.setState({ codeBill: text })}
                                />
                            </View>
                            <Text style={styles.txt}>Hoặc</Text>
                            <TouchableOpacity
                                style={styles.bt}
                                onPress={() => {
                                    this.props.navigation.navigate('scanCode', {
                                        title: 'QUYÉT MÃ ƯU ĐÃI',
                                        setCode: this.setCode.bind(this),
                                        type: 'scanCode',
                                        kind: 'codeBill'
                                    })
                                }}
                            >
                                <Text style={styles.text}>Quét QR</Text>
                            </TouchableOpacity>
                        </View> */}
                        <Dash style={{ width: '100%', height: 1 }} dashGap={7} dashLength={5} dashColor='#DDDDDD' />
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                            {this.state.imgAmount.id ?
                                <Image
                                    style={{ width: api.getRealDeviceWidth() * 0.45, height: api.getRealDeviceWidth() * 0.45, resizeMode: 'contain', margin: 10 }}
                                    source={{ uri: this.state.imgAmount.img }}
                                /> :
                                <Image
                                    style={{ width: api.getRealDeviceWidth() * 0.45, height: api.getRealDeviceWidth() * 0.45, resizeMode: 'contain', margin: 10 }}
                                    source={require('../../img/defaul.png')}
                                />}
                            <TouchableOpacity
                                style={[styles.btScan, { width: '40%', height: 40 }]}
                                onPress={
                                    () => {
                                        Alert.alert(
                                            'Chụp ảnh hoá đơn',
                                            'Tải lên hoá đơn của bạn ',
                                            [
                                                { text: 'Chọn từ album', onPress: () => { this._selectLibrary() } },
                                                { text: 'Chụp ảnh', onPress: () => { this._openCam() } },
                                                { text: 'Đóng', onPress: () => { }, style: 'cancel' },
                                            ],
                                            { cancelable: false }
                                        )


                                    }
                                }

                            >
                                <Text allowFontScaling={false} style={styles.text} >Chụp ảnh hóa đơn</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                    <TouchableOpacity
                        style={[styles.btScan, { marginBottom: 40 }]}
                        onPress={() => this.comfirm()}
                    >
                        <Text style={styles.text}>Xác thực</Text>
                    </TouchableOpacity>
                </ScrollView>
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    bg: {
        width: api.getRealDeviceWidth(),
        height: api.getRealDeviceHeight()
    },
    wrapInput: {
        flexDirection: 'row',
        flex: 6.5,
        backgroundColor: 'white',
        borderRadius: 10,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: '#DDDDDD'
    },
    input: {
        // backgroundColor: 'pink',
        flex: 8,
    },
    icon: {
        flex: 2.5,
        // backgroundColor: 'yellow',
        textAlignVertical: 'center',
        textAlign: 'center',
        color: 'gray',
        marginTop: Platform.OS == 'ios' ? 5 : 0,
        marginBottom: Platform.OS == 'ios' ? 5 : 0

    },
    txt: {
        flex: 1.8,
        textAlign: 'center',
        // color: '',
        fontWeight: 'bold'
    },
    bt: {
        flex: 2,
        backgroundColor: Color.colorPrimari,
        padding: 10,
        borderRadius: 5,
        alignItems: 'center'
    },
    text: {
        color: 'white',
        fontWeight: 'bold'
    },
    btScan: {
        padding: 10,
        backgroundColor: Color.colorPrimari,
        alignItems: 'center',
        borderRadius: 5,
        margin: 10,
    }

})
mapStateToProps = (state) => {
    return {
        token: state.userReducer.token
    }
}
export default connect(mapStateToProps)(ComfirmCode)