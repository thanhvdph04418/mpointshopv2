import React, { Component } from 'react';
import {
    View, Text, ImageBackground, TextInput, StyleSheet,
    TouchableOpacity, Alert, Image, Platform, ScrollView
} from 'react-native';
import { Container, Icon, Header, Left, Body, Right, Title, Button, Input } from 'native-base'
import dataService from '../../network/dataService'
import api from '../../api'
import { Color } from '../../themes/color'
import moment from 'moment'
export default class ComfirmBill extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            dataProduce: [],
            infoOder: null,
            showInfo: false
        }
    }
    async  comfirm() {
        let { code } = this.state;
        if (!code) return api.showMessage('Vui lòng điền Mã hóa đơn')
        api.showLoading()
        let rs = await dataService.checkoutOrderCode(code)
        api.hideLoading();
        if (rs.code != 0) return api.showMessage(rs.msg)
        this.setState({ dataProduce: rs.items, infoOder: rs.orderInfo, showInfo: true })
    }
    setCode(code, kind) {
        console.log('type', kind);
        if (kind == 'code') return this.setState({ code })
    }
    payStatus() {
        if (this.state.infoOder) {
            if (this.state.infoOder.paymentStatus == 1) return 'Đang chờ thanh toán'
            if (this.state.infoOder.paymentStatus == 2) return 'Đang thanh toán'
            if (this.state.infoOder.paymentStatus == 3) return 'Đã thanh toán'
        }
        return ''
    }

    billStatus() {
        if (this.state.infoOder) {
            if (this.state.infoOder.status == 1) return 'Đang chờ'
            if (this.state.infoOder.status == 2) return 'Thành công'
            if (this.state.infoOder.status == 3) return 'Đã hủy'
        }
        return ''
    }


    async  doneOrder() {
        api.showLoading()
        let result = await dataService.doneOrder(this.state.infoOder.id, this.state.txtCode)
        api.hideLoading()
        if (result.code != 0) return api.showMessage(result.msg)
        api.showMessage(result.msg);
    }

    render() {
        return (
            <ImageBackground style={styles.bg} source={require('../../img/bg.png')}>
                <Header
                    androidStatusBarColor={Color.colorStatusbar}
                    style={{ backgroundColor: Color.colorPrimari }}>
                    <Left style={{ flex: 1 }}>
                        <Button onPress={() => { this.props.navigation.pop() }} transparent>
                            <Icon style={{ fontSize: 25, color: '#fff' }} name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 4 }}>
                        <Title style={{ color: '#fff' }}>Xác thực mã hóa đơn</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <View style={styles.wrapInput}>
                            <Icon type='MaterialIcons' name='edit' style={styles.icon} />
                            <TextInput
                                value={this.state.code}
                                style={styles.input}
                                placeholder='Nhập mã hóa đơn'
                                onChangeText={(t) => { this.setState({ code: t }) }}
                            />
                        </View>
                        <Text style={styles.txt}>Hoặc</Text>
                        <TouchableOpacity
                            style={styles.bt}
                            onPress={() => {
                                this.props.navigation.navigate('scanCode', {
                                    title: 'QUYÉT MÃ HÓA ĐƠN',
                                    setCode: this.setCode.bind(this),
                                    type: 'scanCode',
                                    kind: 'code'
                                })
                            }}
                        >
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>Quét QR</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={styles.btScan}
                        onPress={() => this.comfirm()}
                    >
                        <Icon style={{ color: 'white', marginRight: 5 }} type='FontAwesome' name='check' />
                        <Text style={styles.text}>Kiểm tra</Text>
                    </TouchableOpacity>
                    {this.state.showInfo ? <View style={{ backgroundColor: 'white', padding: 10, width: '90%', alignSelf: 'center', borderRadius: 15, marginBottom: 40 }}>
                        <Text style={{ fontSize: 17, fontWeight: '500', textAlign: 'center' }} >Thông tin khách hàng</Text>
                        <Text style={{ fontWeight: '500', }}>Khách hàng: <Text style={{ fontWeight: '300' }} >{this.state.infoOder ? this.state.infoOder.name : ''}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Địa chỉ: <Text style={{ fontWeight: '300' }} >{this.state.infoOder ? this.state.infoOder.address : ''}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Số điện thoại: <Text style={{ fontWeight: '300' }} >{this.state.infoOder ? this.state.infoOder.phone : ''}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Email: <Text style={{ fontWeight: '300' }} >{this.state.infoOder ? this.state.infoOder.email : ''}</Text></Text>

                        <Text style={{ fontSize: 17, fontWeight: '500', marginTop: 20 }} >Thông tin đơn hàng</Text>
                        <Text style={{ fontWeight: '500', }}>Mã đơn hàng: <Text style={{ fontWeight: '300' }} >{this.state.infoOder ? this.state.infoOder.code : ''}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Số tiền thanh toán: <Text style={{ fontWeight: '300' }} >{this.state.infoOder && this.state.infoOder.amount ? this.state.infoOder.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' vnđ' : ''}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Tình trạng đơn hàng: <Text style={{ fontWeight: '300' }} >{this.billStatus()}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Ngày mua hàng: <Text style={{ fontWeight: '300' }} >{this.state.infoOder ? moment(this.state.infoOder.payAt).format('DD-MM-YYYY') : ''}</Text></Text>
                        <Text style={{ fontWeight: '500', }}>Loại thanh toán: <Text style={{ fontWeight: '300' }} ></Text></Text>
                        <Text style={{ fontWeight: '500', }}>Tình trạng thanh toán: <Text style={{ fontWeight: '300' }} ></Text></Text>


                        <Text style={{ fontSize: 17, fontWeight: '500', marginTop: 20 }} >Sản phẩm</Text>
                        {this.state.dataProduce.map((item) => {
                            return <View key={item.id} style={{ paddingLeft: 30, borderBottomWidth: 1, paddingBottom: 5, borderBottomColor: 'gray' }}>
                                <Text style={{}}>{item.product.name}</Text>
                                <Text style={{}}>Số lượng: {item.product.quantity}</Text>
                                <Text style={{}}>Giá bán: {item.product && item.product.price ? item.product.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' vnđ' : ''}</Text>

                            </View>
                        })}

                        <Text style={{ fontSize: 17, fontWeight: '500', marginTop: 20 }} >Mã hóa đơn</Text>
                        <View style={{ borderColor: 'gray', borderWidth: 0.5, marginRight: 15 }}>
                            <Input
                                value={this.state.txtCode}
                                style={{ flex: 1, borderRadius: 10 }}
                                placeholder='Nhập mã hóa đơn' style={{}} placeholderTextColor='gray'
                                onChangeText={(text) => {
                                    this.setState({ txtCode: text })
                                }
                                }

                                keyboardType='phone-pad'
                            />
                        </View>
                        <View style={{ marginRight: 15, flexDirection: 'row', marginTop: 10 }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => { this.doneOrder() }}
                                style={{ borderRadius: 5, flex: 1, backgroundColor: '#fa6428', height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#fff' }}>Hoàn thành đơn</Text>
                            </TouchableOpacity>
                            <View style={{ flex: 0.1 }}></View>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => { this.setState({ showInfo: false }) }}
                                style={{ borderRadius: 5, flex: 1, backgroundColor: 'gray', height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#fff' }}>Hủy đơn</Text>
                            </TouchableOpacity>
                        </View>
                    </View> : null}
                </ScrollView>
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    bg: {
        width: api.getRealDeviceWidth(),
        height: api.getRealDeviceHeight()
    },
    wrapInput: {
        flexDirection: 'row',
        flex: 6.5,
        backgroundColor: 'white',
        borderRadius: 25,
        overflow: 'hidden'
    },
    input: {
        // backgroundColor: 'pink',
        flex: 8
    },
    icon: {
        flex: 2.5,
        // backgroundColor: 'yellow',
        textAlignVertical: 'center',
        textAlign: 'center',
        color: 'gray',
        marginTop: Platform.OS == 'ios' ? 5 : 0,
        marginBottom: Platform.OS == 'ios' ? 5 : 0
    },
    txt: {
        flex: 1.8,
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    bt: {
        flex: 2,
        backgroundColor: Color.colorPrimari,
        padding: 10,
        borderRadius: 25,
        alignItems: 'center'
    },
    text: {
        color: 'white',
        fontWeight: 'bold'
    },
    btScan: {
        padding: 10,
        backgroundColor: Color.colorPrimari,
        alignItems: 'center',
        borderRadius: 25,
        margin: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    }

})
