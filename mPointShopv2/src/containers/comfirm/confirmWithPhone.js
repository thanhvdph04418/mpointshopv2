import React, { Component } from 'react';
import {
    View, Text, ImageBackground, TextInput, StyleSheet,
    TouchableOpacity, Alert, Image, Platform, ScrollView
} from 'react-native';
import { Container, Icon, Header, Left, Body, Right, Title, Button } from 'native-base'
import dataService from '../../network/dataService'
import api from '../../api'
import { Color } from '../../themes/color'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import config from '../../config'
import { connect } from 'react-redux'
import Dash from 'react-native-dash';
var options = {
    title: 'Chọn hoá đơn',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};
class ConfirmWithPhone extends Component {
    constructor(props) {
        super(props);
        let { params } = this.props.navigation.state
        this.state = {
            code: params ? params.code : '',
            totalBill: '',
            codeBill: undefined,
        }
    }
    async  comfirm() {
        let { phone, totalBill, codeBill } = this.state;
        if (!phone) return api.showMessage('Vui lòng nhập số điện thoại')
        let cost = totalBill.replace(/\./g, '')
        if (/[^\d]/.test(cost)) return api.showMessage('Tổng hoá đơn không hợp lệ', 'Thông báo')
        api.showLoading()
        let rs = await dataService.shopCheckoutPhone(phone, totalBill, codeBill)
        api.hideLoading();
        if (rs.code != 0) return api.showMessage(rs.msg)
        api.showMessage(rs.msg)
        this.setState({
            phone: '',
            totalBill: '',
            codeBill: '',

        })
    }
    setCode(code, kind) {
        console.log('type', kind);
        if (kind == 'code') return this.setState({ code })
        if (kind == 'codeBill') return this.setState({ codeBill: code })

    }

    render() {
        return (
            <ImageBackground style={styles.bg} source={require('../../img/bg.png')}>
                <Header
                    androidStatusBarColor={Color.colorStatusbar}
                    style={{ backgroundColor: Color.colorPrimari }}>
                    <Left style={{ flex: 1 }}>
                        <Button onPress={() => { this.props.navigation.pop() }} transparent>
                            <Icon style={{ fontSize: 25, color: '#fff' }} name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 4 }}>
                        <Title style={{ color: '#fff' }}>Xác thực bằng số điện thoại</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ backgroundColor: 'white', width: '95%', alignSelf: 'center', marginTop: 10, overflow: 'hidden' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={styles.wrapInput}>
                                <Icon type='FontAwesome' name='edit' style={[styles.icon, { flex: 1.2 }]} />
                                <TextInput
                                    value={this.state.phone}
                                    style={styles.input}
                                    placeholder='Nhập số điện thoại'
                                    onChangeText={(phone) => {
                                        this.setState({
                                            phone
                                        })
                                    }
                                    }
                                    keyboardType='phone-pad'
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={styles.wrapInput}>
                                <Icon type='FontAwesome' name='dollar' style={[styles.icon, { flex: 1.2 }]} />
                                <TextInput
                                    value={this.state.totalBill.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                    style={styles.input}
                                    placeholder='Tổng hóa đơn (VNĐ)'
                                    onChangeText={(text) => {
                                        let value = '';
                                        if (!value) value = text.replace(new RegExp('\\.', 'g'), '');
                                        this.setState({ totalBill: value })
                                    }
                                    }
                                    keyboardType='phone-pad'
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={styles.wrapInput}>
                                <Icon type='MaterialCommunityIcons' name='qrcode-scan' style={styles.icon} />
                                <TextInput
                                    value={this.state.codeBill}
                                    style={styles.input}
                                    placeholder='Mã hóa đơn (nếu có)'
                                    onChangeText={(text) => this.setState({ codeBill: text })}
                                />
                            </View>
                            <Text style={styles.txt}>Hoặc</Text>
                            <TouchableOpacity
                                style={styles.bt}
                                onPress={() => {
                                    this.props.navigation.navigate('scanCode', {
                                        title: 'QUÉT MÃ HÓA ĐƠN',
                                        setCode: this.setCode.bind(this),
                                        type: 'scanCode',
                                        kind: 'codeBill'
                                    })
                                }}
                            >
                                <Text style={styles.text}>Quét QR</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <Dash style={{ width: '100%', height: 1 }} dashGap={7} dashLength={5} dashColor='#DDDDDD' /> */}

                    </View>
                    <TouchableOpacity
                        style={[styles.btScan, { marginBottom: 40 }]}
                        onPress={() => this.comfirm()}
                    >
                        <Text style={styles.text}>Xác thực</Text>
                    </TouchableOpacity>
                </ScrollView>
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    bg: {
        width: api.getRealDeviceWidth(),
        height: api.getRealDeviceHeight()
    },
    wrapInput: {
        flexDirection: 'row',
        flex: 6.5,
        backgroundColor: 'white',
        borderRadius: 10,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: '#DDDDDD'
    },
    input: {
        // backgroundColor: 'pink',
        flex: 8,
    },
    icon: {
        flex: 2.5,
        // backgroundColor: 'yellow',
        textAlignVertical: 'center',
        textAlign: 'center',
        color: 'gray',
        marginTop: Platform.OS == 'ios' ? 5 : 0,
        marginBottom: Platform.OS == 'ios' ? 5 : 0

    },
    txt: {
        flex: 1.8,
        textAlign: 'center',
        // color: '',
        fontWeight: 'bold'
    },
    bt: {
        flex: 2,
        backgroundColor: Color.colorPrimari,
        padding: 10,
        borderRadius: 5,
        alignItems: 'center'
    },
    text: {
        color: 'white',
        fontWeight: 'bold'
    },
    btScan: {
        padding: 10,
        backgroundColor: Color.colorPrimari,
        alignItems: 'center',
        borderRadius: 5,
        margin: 10,
    }

})
mapStateToProps = (state) => {
    return {
        token: state.userReducer.token
    }
}
export default connect(mapStateToProps)(ConfirmWithPhone)