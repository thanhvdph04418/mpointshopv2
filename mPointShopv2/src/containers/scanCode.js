import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View, Platform,
    PermissionsAndroid
} from 'react-native';
import { Button } from 'native-base';
import api from '../api';
import { RNCamera } from 'react-native-camera';
import { Color } from '../themes/color';
import dataService from '../network/dataService'
import Permissions from 'react-native-permissions'

let scan = true
export default class ScanCode extends Component {
    constructor() {
        super();
        this.state = {
            permission: Platform.OS == 'ios' ? true : false
        }
    }
    componentDidMount() {
        this.checkPermission()
    }
    async checkPermission() {
        if (Platform.OS === 'android' && Platform.Version >= 23) {
            let rs = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
            if (rs) {
                this.setState({ permission: true });
            } else {
                let userApprove = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
                if (userApprove) {
                    this.setState({ permission: true });
                }
            }
        } else {
            let response = await Permissions.checkMultiple(['camera', 'photo']);
            if (response.camera == 'authorized' || response.camera == 'undetermined') {
                this.setState({ permission: true });
            } else {
                Alert.alert(
                    'Thông báo',
                    'Cung cấp quyền camera để có thể quét mã',
                    [{ text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    { text: 'Tới cài đặt', onPress: () => { Permissions.openSettings() } },
                    ],
                    { cancelable: false }
                )
            }
        }
    }
    render() {
        let detectSize = api.getRealDeviceWidth() * 0.9
        if (!this.state.permission) return (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Không được cấp quyền camera.Vui lòng xem lại cài đặt của bạn</Text></View>)
        return (
            <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                onBarCodeRead={this._qrCodeReceive.bind(this)}
                defaultOnFocusComponent={true}
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                captureAudio={false}
                flashMode={RNCamera.Constants.FlashMode.off}
                barCodeTypes={[RNCamera.Constants.BarCodeType.code128, RNCamera.Constants.BarCodeType.qr]}
                // googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.DATA_MATRIX}
                permissionDialogTitle={'vui lòng cho phép camera'}
                permissionDialogMessage={'ứng dụng cần cấp quyền sử dụng camera'}
            >
                <View style={{ backgroundColor: 'rgba(1,1,1,0.45)', padding: 10, bottom: 1, position: 'absolute', width: api.getRealDeviceWidth(), top: ((api.getRealDeviceHeight() - 250) - detectSize) / 2, height: api.getRealDeviceWidth() / 6, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#fff', textAlign: 'center', fontWeight: '500' }}>
                        {this.props.navigation.state.params.title}
                        {/* QUYÉT MÃ THẺ  */}
                    </Text>
                </View>
                <View style={{ borderWidth: 1, borderColor: '#fff', borderRadius: 4, width: api.getRealDeviceWidth() * 0.7, height: api.getRealDeviceWidth() * 0.7 }} />
                <View style={{ padding: 10, bottom: 1, position: 'absolute', width: api.getRealDeviceWidth(), bottom: ((api.getRealDeviceHeight() - 200) - detectSize) / 2 }}>
                    <Button style={styles.btColose} onPress={() => this.props.navigation.pop()}>
                        <Text style={{ color: '#fff' }}>Đóng</Text>
                    </Button>
                </View>
            </RNCamera>
        );
    }
    isReceip = false
    async _qrCodeReceive(code) {
        console.log(code, 'goooooooooo')
        if (!this.isReceip) {
            this.isReceip = true
            let { params } = this.props.navigation.state;
            console.log('params', params)
            if (params.type == 'scanCode') {
                params.setCode(code.data, params.kind);
                await this.props.navigation.pop();
                setTimeout(() => this.isReceip = false, 1500)
            }
            if (params.type == 'scanCardPromtion') {
                // await this.props.navigation.navigate('listpromotion', { id: code.data })
                await this.props.navigation.navigate('listpromotion', { idCard: code.data })
                setTimeout(() => this.isReceip = false, 1500)
            }
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    btColose: {
        borderRadius: 20, paddingRight: 30,
        paddingLeft: 30, backgroundColor: Color.colorSuccess,
        alignSelf: 'center'
    }
});
