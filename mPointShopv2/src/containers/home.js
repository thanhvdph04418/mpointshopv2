import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, Text,
    ImageBackground, SafeAreaView
} from 'react-native';
import api from '../api'
import { Container, Content, Card, CardItem, Body, Left } from 'native-base';

import { Color } from '../themes/color';
import RootHeader from '../emlements/rootHeader';
import { ScrollView } from 'react-native-gesture-handler';
export default class Home extends Component {
    constructor(props) {
        super(props)
        api.setNavigateRouter(this.props.navigation)
    }

    render() {
        console.log(Object.common.getWidth);
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Container styles={styles.container}>
                    {/* <RootHeader /> */}
                    <ScrollView>
                        <Content padder>
                            <Image style={styles.imgLogo} source={require('../img/avatarHome.png')} />
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={styles.TouchItem}
                                // onPress={() => { this.props.navigation.navigate('scanCode') }}
                                onPress={() => {
                                    this.props.navigation.navigate('scanCode', {
                                        title: 'QUÉT THẺ ĐỂ CHỌN ƯU ĐÃI',
                                        type: 'scanCardPromtion',
                                    })
                                }}
                            >
                                <Card >
                                    <CardItem>
                                        <Left >
                                            <Image style={styles.imageIcon} source={require('../img/debit-card.png')} />
                                        </Left>
                                        <Body style={{ flex: 7, marginLeft: 22 }}>
                                            <View style={{ paddingLeft: 10, }}>
                                                <Text style={{ fontSize: 16, color: '#333333' }}>
                                                    QUÉT MÃ THẺ
                                        </Text>
                                                <Text style={{ fontSize: 13, }} >Quét mã thẻ để xem những ưu đãi có trong thẻ của bạn</Text>
                                            </View>
                                        </Body>
                                    </CardItem>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.7} style={styles.TouchItem} onPress={() => this.props.navigation.navigate('comfirmCode')}>
                                <Card >
                                    <CardItem>
                                        <Left >
                                            <Image style={styles.imageIcon} source={require('../img/qr-code.png')} />

                                        </Left>
                                        <Body style={{ flex: 7, marginLeft: 22 }}>
                                            <View style={{ paddingLeft: 10, }}>
                                                <Text style={{ fontSize: 16, color: '#333333' }}>
                                                    XÁC THỰC MÃ ƯU ĐÃI
                                   </Text>
                                                <Text style={{ fontSize: 13, }} >
                                                    Xác thực mã ưu đãi được lấy từ app mPoint
                                   </Text>

                                            </View>
                                        </Body>
                                    </CardItem>
                                </Card>
                            </TouchableOpacity>


                            <TouchableOpacity activeOpacity={0.7} style={styles.TouchItem} onPress={() => this.props.navigation.navigate('comfirmBill')}>
                                <Card >
                                    <CardItem>
                                        <Left >
                                            <Image style={styles.imageIcon} source={require('../img/smartphone.png')} />

                                        </Left>
                                        <Body style={{ flex: 7, marginLeft: 22 }}>
                                            <View style={{ paddingLeft: 10, }}>
                                                <Text style={{ fontSize: 16, color: '#333333' }}>
                                                    XÁC THỰC MÃ HÓA ĐƠN
                                   </Text>

                                                <Text style={{ fontSize: 13, }} >
                                                    Xác thực mã hóa đơn của bạn
                                   </Text>

                                            </View>
                                        </Body>
                                    </CardItem>
                                </Card>
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={0.7} style={styles.TouchItem} onPress={() => this.props.navigation.navigate('confirmWithPhone')}>
                                <Card >
                                    <CardItem>
                                        <Left >
                                            <Image style={styles.imageIcon} source={require('../img/phonenumber.png')} />

                                        </Left>
                                        <Body style={{ flex: 7, marginLeft: 22 }}>
                                            <View style={{ paddingLeft: 10, }}>
                                                <Text style={{ fontSize: 16, color: '#333333' }}>
                                                    XÁC THỰC BẰNG SỐ ĐIỆN THOẠI
                                   </Text>

                                                <Text style={{ fontSize: 13, }} >
                                                    Xác thực bằng số điện thoại của khách hàng
                                   </Text>

                                            </View>
                                        </Body>
                                    </CardItem>
                                </Card>
                            </TouchableOpacity>

                        </Content>
                    </ScrollView>
                    <ImageBackground
                        source={require('../img/bgheader.jpg')}
                        style={{ width: api.getRealDeviceWidth(), height: 200, backgroundColor: Color.colorSuccess, position: 'absolute', zIndex: -1, top: 0 }} >

                    </ImageBackground>
                </Container>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eeeeee',
    },
    TouchItem: {
        marginTop: 15
    },
    imageIcon: {
        width: 60,
        height: 60
    },
    imgLogo: {
        width: api.getRealDeviceWidth() / 3,
        height: api.getRealDeviceWidth() / 3,
        alignSelf: 'center',
    }

});