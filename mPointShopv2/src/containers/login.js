import React, { Component } from 'react';
import { Toast, Container, Header, Content, Thumbnail, Form, Item, Input, Label, Button, Text, View, Icon } from 'native-base';
import {
    ImageBackground, StatusBar, Image, TouchableOpacity, ScrollView, AsyncStorage,
    Modal, StyleSheet, Keyboard
} from 'react-native'
import api from '../api'
import { Color } from '../themes/color'
import dataService from '../network/dataService'
export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            pass: "",
        }
        api.setNavigateRouter(this.props.navigation)
    }
    async login() {
        Keyboard.dismiss();
        let { userName, pass } = this.state;
        if (!userName) return api.showMessage('Vui lòng nhập tài khoản')
        if (!pass) return api.showMessage('Vui lòng nhập mật khẩu')
        api.showLoading();
        let rs = await dataService.loginApp(userName, pass);
        if (rs.code != 0) return api.showMessage(rs.msg)
        api.setToken(rs.token.token);
        api.setUser(rs.user);
        api.setMemberCard(rs.memberCard);
        api.setMemberInfo(rs.memberInfo);
        api.hideLoading()
        api.reset(0, 'bottomTab');
        this.saveToken(rs.token.token)
    }
    async saveToken(token) {
        try {
            await AsyncStorage.setItem('TOKEN', token);
        } catch (er) {
            console.log('err save token', er)
        }
    }
    componentDidMount() {
        this.refreshToken()
    }
    async refreshToken() {
        api.showLoading()
        let token = null;
        try {
            token = await AsyncStorage.getItem('TOKEN');
            console.log('11111111111', token)
            if (token) {
                const result = await dataService.refreshToken(token)
                if (result.err != 0 || result.user.roles.indexOf(4) == -1) {
                    api.hideLoading()
                    return (api.showMessage('Sai thông tin đăng nhập', 'thông báo'));
                }
                api.setToken(token)
                api.setUser(result.user)
                api.setMemberCard(result.memberCard);
                api.setMemberInfo(result.memberInfo);
                api.hideLoading()
                api.reset(0, 'bottomTab');
            } else {
                api.hideLoading()
            }
        } catch (er) {
            console.log('er get token', er)
        }

    }
    render() {
        return (
            <Container>
                <StatusBar backgroundColor={Color.colorStatusbar} />
                <ImageBackground
                    style={{ flex: 1, alignItems: 'center' }}
                    source={require('../img/bg.png')}
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                    >

                        <Image style={{
                            width: api.getRealDeviceWidth() / 2.5,
                            height: api.getRealDeviceWidth() / 2.5,
                            marginTop: api.getRealDeviceHeight() / 5,
                            alignSelf: 'center',
                            resizeMode: 'contain',
                        }} source={require('../img/avatarHome.png')} />

                        <View style={{ marginTop: 20, alignItems: 'center' }} >
                            <Item
                                style={{
                                    backgroundColor: 'rgba(248,248,255,0.5)',
                                    borderColor: 'transparent',
                                    width: api.getRealDeviceWidth() - 50,
                                    borderRadius: 5
                                }}
                            >
                                <Icon type='FontAwesome5' name='user' style={{ color: '#FF7043', marginLeft: 10 }} />
                                <View style={{ width: 2, backgroundColor: '#FF7043', height: '70%', marginLeft: 6 }} />
                                <Input
                                    autoCapitalize='none'
                                    value={this.state.userName}
                                    onChangeText={(userName) => { this.setState({ userName }) }}
                                    selectionColor={'#fff'}
                                    placeholderTextColor='#B0BEC5'
                                    style={{
                                        // textAlign: 'center',
                                        marginLeft: 8,
                                        color: Color.colorPrimari
                                    }} placeholder='Tên đăng nhập'
                                    onSubmitEditing={() => { this.inputPass._root.focus() }}
                                />
                            </Item>
                            <Item
                                style={{
                                    backgroundColor: 'rgba(248,248,255,0.5)',
                                    borderColor: 'transparent',
                                    width: api.getRealDeviceWidth() - 50,
                                    marginTop: 5,
                                    borderRadius: 5

                                }}
                            >
                                <Icon type='MaterialIcons' name='lock-outline' style={{ color: '#FF7043', marginLeft: 10, fontSize: 28 }} />
                                <View style={{ width: 2, backgroundColor: '#FF7043', height: '70%' }} />
                                <Input
                                    ref={component => this.inputPass = component}
                                    autoCapitalize='none'
                                    secureTextEntry={true}
                                    value={this.state.pass}
                                    onChangeText={(pass) => { this.setState({ pass }) }}
                                    selectionColor={'#fff'}
                                    placeholderTextColor='#B0BEC5'
                                    placeholder='Mật khẩu'
                                    style={{
                                        // textAlign: 'center',
                                        color: Color.colorPrimari,
                                        marginLeft: 8,
                                    }}
                                    onSubmitEditing={() => { this.login() }}

                                />
                            </Item>
                            <Button
                                // onPress={() => { this.props.navigation.navigate('bottomTab') }}
                                onPress={() => { this.login() }}
                                block light style={{ backgroundColor: '#fff', borderRadius: 5, height: 50, marginTop: 15, width: api.getRealDeviceWidth() - 50, alignSelf: 'center' }}>
                                <Text allowFontScaling={false} style={{ color: Color.colorPrimari, fontWeight: 'bold', backgroundColor: 'transparent' }}>Đăng nhập</Text>
                            </Button>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </Container>
        );
    }
}
var styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
});