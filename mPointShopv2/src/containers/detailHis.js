import React, { Component } from 'react';
import {
    View, Text, Image, ActivityIndicator, StyleSheet,
    ImageBackground, ScrollView
} from 'react-native';
import moment from 'moment'
import { Container, Icon, Header, Left, Body, Right, Title, Button } from 'native-base'
import dataService from '../network/dataService'
import api from '../api'
import { Color } from '../themes/color'
export default class DetailHis extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }
    componentDidMount() {
        this.getDetail()
    }

    async getDetail() {
        let { id } = this.props.navigation.state.params
        const result = await dataService.shopGetHistoryInfo(id)
        console.log(result)
        if (result.code != 0) return api.showMessage(result.msg)
        this.setState({
            data: result.data
        })
    }
    render() {
        let { data } = this.state;
        let { name, image } = this.props.navigation.state.params
        if (!data) {
            return (
                <Container>
                    <ActivityIndicator size='large' style={{ color: 'red', alignSelf: 'auto', marginTop: 20 }} />
                </Container>
            )
        }
        return (
            <ImageBackground
                style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() }} source={require('../img/bg.png')}>
                <ScrollView>
                    <Header
                        androidStatusBarColor={Color.colorStatusbar}
                        style={{ backgroundColor: Color.colorPrimari }}>
                        <Left>
                            <Button onPress={() => { this.props.navigation.pop() }} transparent>
                                <Icon style={{ color: '#fff' }} name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title style={{ color: 'white' }}>Chi tiết</Title>
                        </Body>
                        <Right>

                        </Right>
                    </Header>
                    <View style={styles.wrap}>
                        <Image style={styles.logo} source={{ uri: image }} />
                        <Text style={styles.name}>{name}</Text>
                        <Text style={styles.content}>{data.promotion.name}</Text>
                        <View style={styles.line}>
                            <Text style={styles.left}>Mã xác thực:</Text>
                            <Text style={styles.right}>{data.code}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.left}>Người dùng:</Text>
                            <Text style={styles.right}>{data.user.name}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.left}>Số điện thoại: </Text>
                            <Text style={styles.right}>{data.user.phone}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.left}>Ngày lấy mã:</Text>
                            <Text style={styles.right}>{moment(data.checkoutAt).format('DD-MM-YYYY')}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.left}>Hết hạn: </Text>
                            <Text style={styles.right}>{moment(data.endDate).format('DD-MM-YYYY')}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.left}>Hóa đơn: </Text>
                            <Text style={styles.right}>{data.billAmount ? (data.billAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' vnđ') : 'không có hóa đơn'}</Text>
                        </View>
                        <Image style={styles.imgReceip} source={{ uri: data.receiptImage }} />
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    logo: {
        width: 140,
        height: 140,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    imgReceip: {
        width: 120,
        height: 120,
        resizeMode: 'contain',
        marginTop: 10
    },
    wrap: {
        width: '96%',
        padding: 20,
        backgroundColor: 'rgba(245,245,245 ,0.6)',
        alignSelf: 'center',
        borderRadius: 10,
    },
    name: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        margin: 8
    },
    content: {
        fontWeight: 'bold'
    },
    line: {
        flexDirection: 'row',
        marginTop: 3
    },
    left: {
        flex: 4,
        fontWeight: 'bold'
    },
    right: {
        flex: 6
    }
})