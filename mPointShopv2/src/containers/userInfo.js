
import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, Image, ScrollView, Text,
    TouchableOpacity, ImageBackground, Alert, Modal, TextInput, SafeAreaView, AsyncStorage
} from 'react-native'
import { Container, Icon, ListItem, List, Right, Body, Left, Button, Switch } from 'native-base'
import api from '../api';
import { Color } from '../themes/color';
import { connect } from 'react-redux';
import dataService from '../network/dataService'
import DeviceInfo from 'react-native-device-info';
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMDChagepass: false,
            oldPassword: '',
            newPassword: '',
            confPassword: '',
            errChangePass: ''
        }
    }
    onlogout() {
        Alert.alert(
            'Thông báo',
            'Bạn có muốn đăng xuất ?',
            [
                { text: 'Huỷ', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Đăng xuất', onPress: async () => {
                        try {
                            await AsyncStorage.removeItem('TOKEN');
                        } catch (error) {
                            console.log('Error when remove token', error);
                        }
                        api.reset(0, 'login')
                        api.setToken('')
                    }
                },
            ],
            { cancelable: false }
        )
    }
    async changePass() {
        this.setState({ errChangePass: '' })
        if (this.state.oldPassword.length == 0) return this.setState({ errChangePass: 'Vui lòng nhập mật khẩu cũ' })
        if (this.state.newPassword.length == 0) return this.setState({ errChangePass: 'Vui lòng nhập mật khẩu mới' })
        if (this.state.confPassword.length == 0) return this.setState({ errChangePass: 'Vui lòng xác nhận mật khẩu' })
        if (this.state.newPassword !== this.state.confPassword) return this.setState({ errChangePass: 'Mật khẩu mới và mật khẩu xác nhận phải giống nhau' })
        let chagePass = await dataService.userChangePassword(this.state.oldPassword, this.state.newPassword)
        if (chagePass.code != 0) return this.setState({ errChangePass: chagePass.msg })
        this.setState({ showMDChagepass: false, oldPassword: '', newPassword: '', confPassword: '' })
        api.showMessage('Cập nhật mật khẩu thành công')
    }

    modalChangePass() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.showMDChagepass}
                onRequestClose={() => {
                    this.setState({ showMDChagepass: false })
                }}>
                <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() }}>
                    <View style={{ backgroundColor: '#fff', width: api.getRealDeviceWidth() - 30, alignSelf: 'center', marginTop: 100, borderRadius: 5, paddingBottom: 15, paddingTop: 10 }}>
                        <View style={{
                            width: api.getRealDeviceWidth() - 30, height: 30, borderBottomColor: 'gray', borderBottomWidth: 0.5, alignItems: 'center', justifyContent: 'center',
                            paddingBottom: 10
                        }} >
                            <Text style={{ color: '#111111', fontSize: 17 }}>Đổi mật khẩu</Text>
                        </View>
                        <View>
                            <View >
                                <TextInput
                                    onChangeText={(oldPassword) => { this.setState({ oldPassword }) }}
                                    textContentType='password'
                                    secureTextEntry={true}
                                    style={{ width: api.getRealDeviceWidth() - 50, height: 40, borderColor: '#dddddd', borderWidth: 1, marginTop: 5, borderRadius: 5, alignSelf: 'center', textAlign: 'center' }}
                                    placeholder='nhập mật khẩu cũ (*)'
                                />
                                <TextInput
                                    onChangeText={(newPassword) => { this.setState({ newPassword }) }}
                                    textContentType='password'
                                    secureTextEntry={true}
                                    style={{ width: api.getRealDeviceWidth() - 50, height: 40, borderColor: '#dddddd', borderWidth: 1, marginTop: 5, borderRadius: 5, alignSelf: 'center', textAlign: 'center' }}
                                    placeholder='nhập mật khẩu mới (*)'
                                />
                                <TextInput
                                    onChangeText={(confPassword) => { this.setState({ confPassword }) }}
                                    textContentType='password'
                                    secureTextEntry={true}
                                    style={{ width: api.getRealDeviceWidth() - 50, height: 40, borderColor: '#dddddd', borderWidth: 1, marginTop: 5, borderRadius: 5, alignSelf: 'center', textAlign: 'center' }}
                                    placeholder='Xác nhận lại mật khẩu (*)'
                                />
                            </View>

                            <Text style={{ color: 'red', textAlign: 'center', }} >{this.state.errChangePass}</Text>
                            <View style={{ flexDirection: 'row', width: api.getRealDeviceWidth() - 30, alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => { this.setState({ showMDChagepass: false }) }}
                                    style={{ minWidth: 70, alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }} >
                                    <Text style={{ color: '#fff' }}>Đóng</Text>
                                </TouchableOpacity>
                                <View style={{ width: 30 }} />
                                <TouchableOpacity
                                    onPress={() => { this.changePass() }}
                                    style={{ alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }} >
                                    <Text style={{ color: '#fff' }}>Đổi mật khẩu</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                </View>
            </Modal>
        )
    }
    render() {

        let { user, memberInfo, memberCard } = this.props
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Container >
                    {this.modalChangePass()}
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <ImageBackground source={require('../img/bgheader.jpg')} style={{ width: '100%', height: 250, justifyContent: 'center', alignItems: 'center' }} >
                            <TouchableOpacity
                                style={{
                                    width: api.getRealDeviceWidth() / 3,
                                    height: api.getRealDeviceWidth() / 3,
                                    borderRadius: api.getRealDeviceWidth() / 6,
                                    alignSelf: 'center',
                                    marginTop: 20,
                                    overflow: 'hidden'

                                }}
                                activeOpacity={1}
                            >
                                <Image
                                    style={{
                                        width: api.getRealDeviceWidth() / 3,
                                        height: api.getRealDeviceWidth() / 3,
                                        resizeMode: 'contain',
                                    }}
                                    source={{ uri: user.avatar }}
                                />
                                {/* <View style={{ alignSelf: 'center', position: 'absolute', width: 30, height: 30, backgroundColor: '#fff', zIndex: 10, justifyContent: 'center', alignItems: 'center', borderRadius: 15, top: 15, right: 10 }}>
                                    <Icon style={{ fontSize: 23 }} type='EvilIcons' name='camera' />
                                </View> */}
                            </TouchableOpacity>

                            <Text style={{ fontWeight: 'bold', color: '#333333', fontSize: 16, marginTop: 5, }}>{user.name} | ID: {user.id}</Text>
                            <Text style={{ color: '#444444', fontSize: 15, marginTop: 3, }}>
                                {memberInfo ? memberInfo.email : 'Chưa liên kết email'}
                            </Text>
                            {/* <Text style={{ color: '#444444', fontSize: 15, marginTop: 3, }}>
                                ID: {user.id}
                            </Text> */}

                            <View style={{ flexDirection: 'row', marginTop: 10, alignSelf: 'center' }}>
                                <View style={{ justifyContent: 'center', paddingRight: 5 }}>
                                    <Text style={{ color: 'yellow', fontWeight: 'bold', fontSize: 20 }}>{memberCard ? memberCard.point : 0} điểm</Text>
                                </View>
                            </View>
                        </ImageBackground>
                        <View style={{ width: api.getRealDeviceWidth(), marginTop: 30 }} >
                            <ListItem icon onPress={() => this.setState({ showMDChagepass: true })}>
                                <Left>
                                    <Icon type='Foundation' name="key" />
                                </Left>
                                <Body>
                                    <Text>Đổi mật khẩu</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon onPress={() => this.props.navigation.navigate('changeAccConnect')}>
                                <Left style={{}}>
                                    <Icon type='FontAwesome5' name='exchange-alt' style={{ fontSize: 22 }} />
                                </Left>
                                <Body>
                                    <Text>Đổi tài khoản liên kết</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon onPress={() => this.props.navigation.navigate('tabHis')} >
                                <Left>
                                    <Icon type='Entypo' active name="clock" style={{ fontSize: 22 }} />
                                </Left>
                                <Body>
                                    <Text>Lịch sử đối xoát</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <View style={{ width: api.getRealDeviceWidth() - 20, height: 3, backgroundColor: '#eeeeee', marginTop: 5, alignSelf: 'center' }} />
                            <ListItem icon onPress={() => this.onlogout()} >
                                <Left>
                                    <Icon type='MaterialCommunityIcons' active name="logout-variant" />
                                </Left>
                                <Body>
                                    <Text>Đăng xuất</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon  >
                                <Body>
                                    <Text>Version {Platform.OS == 'ios' ? 'IOS' : "Android"} {DeviceInfo.getVersion()}</Text>
                                </Body>

                            </ListItem>
                        </View>
                    </ScrollView>
                </Container >
            </SafeAreaView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        memberInfo: state.userReducer.memberInfo,
        memberCard: state.userReducer.memberCard
    }
}
export default connect(mapStateToProps)(Profile)


