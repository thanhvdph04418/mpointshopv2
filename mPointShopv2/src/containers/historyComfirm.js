import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image, Text,
    TouchableOpacity,
    ActivityIndicator,
    Platform, SafeAreaView
} from 'react-native';
// import { Icon, Container } from 'native-base'
import moment from 'moment'
import dataService from '../network/dataService'
import api from '../api'
import { Container, Icon, Header, Left, Body, Right, Title, Button } from 'native-base'
import { Color } from '../themes/color'

export default class HistoryComfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataHis: [],
            skip: 0,
            limit: 10,
            stopLoad: false,
            refreshing: false
        }
    }
    componentDidMount() {
        this.getData()
    }
    isLoading = false
    async getData() {
        await this.setState({
            refreshing: true,
            stopLoad: false,
            dataHis: []
        })
        let rs = await dataService.shopGetHistories(this.state.skip, this.state.limit);
        if (rs.code != 0) { return console.log(rs.msg) }
        this.setState({
            dataHis: rs.data,
            refreshing: false,
            stopLoad: rs.data.length < this.state.limit ? true : false
        })
    }
    async getMoreData() {
        let rs = await dataService.shopGetHistories(this.state.dataHis.length, this.state.limit);
        if (rs.code !== 0) { return }
        this.state.dataHis = this.state.dataHis.concat(rs.data);
        console.log('aaa', this.state.dataHis)
        this.setState({
            stopLoad: rs.data.length < this.state.limit ? true : false,
            dataHis: this.state.dataHis
        })

    }
    render() {
        return (
            // <SafeAreaView style={{ flex: 1 }}>
            <Container style={{ paddingTop: (Platform.OS == 'ios' ? 0 : 0) }}>
                <Header
                    androidStatusBarColor={Color.colorStatusbar}
                    style={{ backgroundColor: Color.colorPrimari }}>
                    <Left style={{ flex: 1 }}>

                    </Left>
                    <Body style={{ flex: 4, alignItems: 'center' }}>
                        <Title style={{ color: 'white', textAlign: 'center' }}>Lịch sử đối soát</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>
                <FlatList
                    extraData={this.state}
                    data={this.state.dataHis}
                    onEndReachedThreshold={0.3}
                    onEndReached={this.state.stopLoad ? null : this.getMoreData.bind(this)}
                    refreshing={Platform.OS == 'ios' ? false : this.state.refreshing}
                    onRefresh={() => this.getData()}
                    ListEmptyComponent={
                        !this.state.refreshing ? <Text style={{ alignSelf: 'center', marginTop: 15 }}>Không có dữ liệu</Text> : null
                    }
                    ListFooterComponent={
                        !this.state.refreshing && !this.state.stopLoad ? <ActivityIndicator size='large' style={{ color: 'red' }} /> : null
                    }
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity
                                style={styles.wrapItem}
                                onPress={() => this.props.navigation.navigate('detailHis', {
                                    id: item.id,
                                    name: item.partner.name,
                                    image: item.partner.logo
                                })}
                            >
                                <Image style={styles.avatar} source={{ uri: item.partner.logo }} />
                                <View style={styles.left}>
                                    <Text style={styles.name}>{item.partner.name}</Text>
                                    <Text style={styles.content}>{item.promotion.name}</Text>
                                    <Text >Mã xác thực: <Text style={styles.code}>{item.code}</Text></Text>
                                    <Text style={styles.date}>Ngày đối soát: {moment(item.checkoutAt).format('DD-MM-YYYY')}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item, index) => 'index' + index}
                />
            </Container>
            // </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    wrapItem: {
        flexDirection: 'row',
        marginTop: 10
    },
    avatar: {
        width: 80,
        height: 80,
        // resizeMode: 'contain',
        borderRadius: 5,
        margin: 10,
    },
    left: {
        flex: 7,
        borderBottomWidth: 1,
        borderColor: '#CCCCCC'
    },
    name: {
        fontWeight: '500',
        fontSize: 17
    },
    code: {
        fontWeight: '500'
    },
    date: {
        textAlign: 'right',
        fontStyle: 'italic',
        margin: 5,
        marginRight: 10
    }
})