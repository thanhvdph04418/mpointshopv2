import React, { Component } from 'react';
import {
    View, Text, TextInput, TouchableOpacity, Modal, Alert, ImageBackground, StyleSheet
} from 'react-native';
import { Container, Icon, Header, Left, Body, Right, Title, Button } from 'native-base'
import dataService from '../network/dataService'
import api from '../api'
import { Color } from '../themes/color'
import { connect } from 'react-redux'
class ChangeAccConnect extends Component {
    constructor(props) {
        super(props)
        this.state = {
            phone: '',
            modalVisible: false,
            otp: '',
            idOtp: null,
            noneOtp: false,
            hasMemberCard: this.props.memberCard ? true : false,
            showBtnBack: false

        }
    }
    componentDidMount() {
        this.setState({
            hasMemberCard: this.props.memberCard ? true : false,
        })
    }


    async confirmOTP() {
        if (!this.state.otp) return this.showAlert('Vui lòng nhập OTP')

        if (!Number.isInteger(Number(this.state.otp))) return this.showAlert('OTP phải là số')
        let result = await dataService.confirmOTP(this.state.idOtp, this.state.otp);
        console.log(result)
        if (result.code != 0) {
            return this.showAlert(result.msg)
        }
        this.setState({ otpId: '', otp: '' })
        let mergeShopUser = await dataService.mergeShopUser(result.data.token)
        if (mergeShopUser.code != 0) return this.showAlert(mergeShopUser.msg)
        api.setMemberCard(mergeShopUser.card);
        api.setMemberInfo(mergeShopUser.memberInfo)
        await this.setState({ hasMemberCard: true, showBtnBack: false, modalVisible: false });
        setTimeout(() => {
            api.showMessage('Chúc mùng bạn đã gắn thẻ thành công')
        }, 1000)



    }
    showAlert(title) {
        Alert.alert(
            'Thông báo',
            title,
            [
                { text: 'Đóng', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false })
    }
    async opentInputOtp() {
        if (!this.state.phone) return this.showAlert('Vui lòng nhập số điện thoại')
        if (!Number.isInteger(Number(this.state.phone))) return this.showAlert('Vui lòng nhập số điện thoại')
        api.showLoading()
        let result = await dataService.createOtp(this.state.phone, this.props.token)
        api.hideLoading()
        if (result.code != 0) return this.showAlert(result.msg)
        this.setState({
            idOtp: result.otpId,
            modalVisible: true,
            noneOtp: false
        })
    }
    renderContent() {
        if (!this.state.hasMemberCard) {
            return (
                <View>
                    <View style={{ padding: 10 }}>
                        <Text style={{ backgroundColor: 'transparent', color: '#fff' }}>Để thực hiện gắn mã thẻ tích điểm. Bạn cần xác thực số điện thoại qua 2 bước:</Text>
                        <Text style={{ backgroundColor: 'transparent', marginLeft: 20, color: '#fff' }}>1.Nhập số điện thoại để xác thực </Text>
                        <Text style={{ backgroundColor: 'transparent', marginLeft: 20, color: '#fff' }}>2.Nhập mã xác thực (otp) nhận được trên điện thoại </Text>
                    </View>
                    {this.state.noneOtp ? <Text style={{ alignSelf: 'center', textAlign: 'center', color: 'red', backgroundColor: 'transparent' }}>Kiểm tra lại số điện thoại của bạn và thử lại</Text> : null}
                    <View style={{
                        width: api.getRealDeviceWidth() - 30,
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 40,
                        backgroundColor: 'white',
                        borderRadius: 5,
                        overflow: 'hidden',
                        marginTop: 10,
                        alignSelf: 'center'
                    }}>
                        <TextInput
                            style={{ height: 40, width: api.getRealDeviceWidth() - 30, textAlign: 'center' }}
                            placeholder='Nhập số điện thoại'
                            onChangeText={(phone) => { this.setState({ phone }) }}
                            keyboardType='phone-pad'
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => this.opentInputOtp()}
                        style={{ alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }} >
                        <Text style={{ color: '#fff' }}>Gửi mã xác nhận</Text>
                    </TouchableOpacity>
                    {this.state.showBtnBack ? <TouchableOpacity
                        onPress={() => {
                            this.setState({ hasMemberCard: true, showBtnBack: false })
                        }}
                        style={{ alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }} >
                        <Text style={{ color: '#fff' }}>Quay lại</Text>
                    </TouchableOpacity> : null}
                </View>
            )
        }
        return (
            <View style={{ width: api.getRealDeviceWidth(), alignItems: 'center', marginTop: 50 }}>
                <View style={{ width: api.getRealDeviceWidth() - 70, backgroundColor: '#fff', alignItems: 'center', borderRadius: 10, overflow: 'hidden', }}>
                    <View style={{ width: api.getRealDeviceWidth() - 90, alignItems: 'center', borderBottomColor: '#dddddd', borderBottomWidth: 1, paddingTop: 5, paddingBottom: 5 }} >
                        <Text style={{ fontSize: 17, color: '#111111' }}>Tài khoản đã liên kết</Text>
                    </View>
                    <View style={{ width: api.getRealDeviceWidth() - 70, paddingLeft: 10, bottom: 10 }}>
                        <Text style={{ fontSize: 16, marginTop: 10, color: '#333333' }}>Tên: {this.props.userState.name}</Text>
                        {/* <Text style={{ fontSize: 16, marginTop: 10, color: '#333333' }}>Số điện thoại: {this.props.memberInfo.phone}</Text> */}
                        <Text style={{ fontSize: 16, marginTop: 10, color: '#333333' }}>Email : {this.props.memberInfo.email}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => { this.setState({ hasMemberCard: false, showBtnBack: true }); }}
                        activeOpacity={0.8}
                        style={{ marginBottom: 5, alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }}
                    >
                        <Text style={{ color: '#fff' }}>Thay đổi tài khoản liên kết</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    render() {
        return (
            <ImageBackground source={require('../img/bg.png')} style={styles.bg}>
                <Modal

                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: false })
                    }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', flex: 1 }}>
                        <View style={{ backgroundColor: '#fff', width: api.getRealDeviceWidth() - 30, alignSelf: 'center', marginTop: 200, borderRadius: 5, paddingBottom: 15, paddingTop: 10 }}>
                            <View style={{ width: api.getRealDeviceWidth() - 30, height: 20, borderBottomColor: 'gray', borderBottomWidth: 0.5, alignItems: 'center' }} >
                                <Text>Nhập mã OTP</Text>
                            </View>
                            <Text style={{ alignSelf: 'center', textAlign: 'center', marginTop: 10 }} > Một mã OTP đã được gửi tới số điện thoại: {this.state.phone}</Text>
                            <View>
                                <View style={{
                                    width: api.getRealDeviceWidth() - 40,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                    height: 40,
                                    backgroundColor: 'white',
                                    borderRadius: 5,
                                    overflow: 'hidden',
                                    marginTop: 10,
                                    alignSelf: 'center',
                                    borderColor: '#111111', borderWidth: 0.5,
                                }}>
                                    <TextInput
                                        autoFocus={true}
                                        style={{ flex: 1, textAlign: 'center', }}
                                        placeholder='Nhập mã OTP để xác nhận'
                                        onChangeText={(otp) => { this.setState({ otp }) }}
                                        keyboardType='phone-pad'
                                        value={this.state.otp}
                                    />

                                </View>
                                <Text
                                    onPress={() => { this.setState({ modalVisible: false, noneOtp: true }); }}
                                    style={{ alignSelf: 'center', textAlign: 'center', marginTop: 10, color: '#fa6428' }} >Không nhận được mã xác nhận ? </Text>
                                <View style={{ flexDirection: 'row', width: api.getRealDeviceWidth() - 40, justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ otpId: '', otp: '', modalVisible: false }) }}
                                        style={{ minWidth: 70, alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }} >
                                        <Text style={{ color: '#fff' }}>Đóng</Text>
                                    </TouchableOpacity>
                                    <View style={{ width: 25 }} />
                                    <TouchableOpacity
                                        onPress={() => { this.confirmOTP() }}
                                        style={{ alignSelf: 'center', marginTop: 10, justifyContent: 'center', alignItems: 'center', height: 30, backgroundColor: '#fa6428', padding: 5, borderRadius: 5 }} >
                                        <Text style={{ color: '#fff' }}>Xác nhận</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Header
                    androidStatusBarColor={Color.colorStatusbar}
                    style={{ backgroundColor: Color.colorPrimari }}>
                    <Left style={{ flex: 1 }}>
                        <Button onPress={() => { this.props.navigation.pop() }} transparent>
                            <Icon style={{ color: '#fff', fontSize: 26 }} name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 5 }}>
                        <Title style={{ color: '#fff' }}>Thay đổi tài khoản liên kết</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>
                {this.renderContent()}
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    bg: {
        width: api.getRealDeviceWidth(),
        height: api.getRealDeviceHeight()
    }
})
mapStateToProps = (state) => {
    return {
        memberCard: state.userReducer.memberCard,
        memberInfo: state.userReducer.memberInfo,
        userState: state.userReducer.user
    }
}

export default connect(mapStateToProps)(ChangeAccConnect)