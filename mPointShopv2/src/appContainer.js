
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Button, NetInfo } from 'react-native';
import { createStackNavigator, createAppContainer, createDrawerNavigator } from 'react-navigation'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Home from './containers/home'
import Login from './containers/login';
import { Icon } from 'native-base'
import Drawer from './components/drawer'
import { color, Color } from './themes/color'
import { Loading, ConfirmBox, Messagebox } from './ui'
import ScanCode from './containers/scanCode'
import HistoryComfirm from './containers/historyComfirm'
import DetailHis from './containers/detailHis'
import UserInfo from './containers/userInfo'
import ChangeAccConnect from './containers/changeAccConnect'
import ComfirmCode from './containers/comfirm/comfirmCode'
import ConfirmWithPhone from './containers/comfirm/confirmWithPhone'
import SplashScreen from 'react-native-splash-screen'
import ComfirmBill from './containers/comfirm/comfirmBill'
import Listpromotion from './containers/promotion/listpromotion'
import api from './api';

let firstRouter = false


const RootTab = createMaterialBottomTabNavigator({
    tabhome: {
        screen: Home,
        navigationOptions: {
            tabBarLabel: 'Xác thực',
            tabBarIcon: ({ focused, horizontal, tintColor }) => (<Icon style={{ color: tintColor, fontSize: 25 }} name='md-barcode' />)
        }
    },
    tabHis: {
        screen: HistoryComfirm,
        navigationOptions: {
            tabBarLabel: 'Lịch sử',
            tabBarIcon: ({ focused, horizontal, tintColor }) => (<Icon style={{ color: tintColor, fontSize: 26 }} name='md-clock' />)
        }
    },
    tabAcc: {
        screen: UserInfo,
        navigationOptions: {
            tabBarLabel: 'Tài khoản',
            tabBarIcon: ({ focused, horizontal, tintColor }) => (<Icon style={{ color: tintColor, fontSize: 26 }} name='md-contact' />)
        }
    },

}, {
        shifting: true,
        initialRouteName: 'tabhome',
        activeColor: Color.colorPrimari,
        inactiveColor: 'gray',
        barStyle: { backgroundColor: '#fff', borderTopWidth: Platform.OS == 'ios' ? 1 : null, borderTopColor: '#eeeeee' },
        backBehavior: 'none'
    });






const RootStack = createStackNavigator({
    login: {
        screen: Login
    },
    bottomTab: {
        screen: RootTab
    },
    scanCode: {
        screen: ScanCode
    },
    detailHis: {
        screen: DetailHis
    },
    changeAccConnect: {
        screen: ChangeAccConnect
    },
    comfirmCode: {
        screen: ComfirmCode
    },
    confirmWithPhone: {
        screen: ConfirmWithPhone
    },
    comfirmBill: {
        screen: ComfirmBill
    },
    listpromotion: {
        screen: Listpromotion
    },

}, {
        initialRouteName: 'login',
        headerMode: 'none'
    })

const RootDrawer = createDrawerNavigator({
    Home: {
        screen: RootStack,
    },
}, {
        contentComponent: props => <Drawer {...props} />
    });

const RootApp = createAppContainer(RootStack)

export default class AppContainer extends Component {

    handleNavigationChange = (prevState, newState, action) => {
        if (newState.index == 0) {
            firstRouter = true
        } else {
            firstRouter = false
        }
        ///handle backAndroid///
    }

    componentDidMount() {
        SplashScreen.hide();
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }
    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }
    handleConnectivityChange = isConnected => {
        console.log('jjjjjjjjjjjjj', isConnected)
        if (!isConnected) {
            api.showMessage('Vui lòng kiểm tra kết nối internet!')
        }
    };

    render() {
        return (
            <View style={{ flex: 1 }}>


                {/* Container */}
                <RootApp
                    onNavigationStateChange={this.handleNavigationChange}
                    uriPrefix="/app/thanhvd" />

                {/* UI */}
                <ConfirmBox />
                <Loading />
                <Messagebox />

            </View>)
    }
}
