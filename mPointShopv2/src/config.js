const config = {
    // HOST: 'http://prod.mpoint.vn/'
    // HOST: 'http://staging.mpoint.vn/'
    HOST: __DEV__ ? 'http://staging.mpoint.vn/' : 'http://prod.mpoint.vn/',
}
export default config;