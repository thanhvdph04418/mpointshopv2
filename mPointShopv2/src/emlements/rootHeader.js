import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Header, Left, Right, Body, Icon, Button } from 'native-base'
import { withNavigation } from 'react-navigation'
import { Color } from '../themes/color';
import { DrawerActions } from 'react-navigation-drawer';
class RootHeader extends Component {

    toggeDrawer = () => {
        this.props.navigation.dispatch(DrawerActions.toggleDrawer())
    }
    render() {
        return (
            <Header
                androidStatusBarColor={Color.colorStatusbar}
                style={styles.header}
            >
                <Left>
                    <Button
                        onPress={this.toggeDrawer}
                        transparent>
                        <Icon name='menu' />
                    </Button>
                </Left>
                <Body>

                </Body>
                <Right />

            </Header>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: Color.colorPrimari
    },

});


export default withNavigation(RootHeader)