import { Component } from 'react'
import { Dimensions } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'
let nav = null
store = null
let api = {
    setStore: (newStore) => {
        store = newStore
    },
    getRealDeviceHeight: () => {
        return (Dimensions.get('screen').height)
    },
    getRealDeviceWidth: () => {
        return (Dimensions.get('screen').width)
    },
    setNavigateRouter: (navigation) => {
        nav = navigation
    },
    getToken: () => { return (store ? store.getState().userReducer.token : '') },
    setToken: (token) => { store.dispatch({ type: 'SET_TOKEN', token }) },
    setUser: (user) => { store.dispatch({ type: 'SET_USER_INFO', user }) },
    setMemberCard: (memberCard) => { store.dispatch({ type: 'SET_MEMBER_CARD', memberCard }) },
    setMemberInfo: (memberInfo) => { store.dispatch({ type: 'SET_MEMBER_INFO', memberInfo }) },
    navigate: (routeName, params, action) => {
        nav.dispatch(
            NavigationActions.navigate({
                routeName,
                params,
                action: action,
            })
        )
    },
    reset: (index, route) => {
        nav.dispatch(
            StackActions.reset({
                index: index,
                actions: [NavigationActions.navigate({ routeName: route })],
            })
        )
    },
    pop: () => {
        nav.pop()
    },
    replace: (key, newKey, routeName, params, action) => {
        nav.dispatch(
            StackActions.replace({
                key,
                newKey,
                routeName,
                params,
                action
            })
        )
    },
    getNavigationState: () => {
        return nav;
    },
    showLoading: () => {
        store.dispatch({ type: 'SHOW_LOADING' })
    },
    hideLoading: () => {
        store.dispatch({ type: 'HIDE_LOADING' })
    },
    showMessage: (content, title) => {
        store.dispatch({ type: 'SHOW_MESSAGE', content, title })
    },
    hideMessage: (content, title) => {
        store.dispatch({ type: 'HIDE_MESSAGE' })
    },
    showConfirm: (content, title, btnHuy, btnOK, titlebntHuy, titlebntOK) => {
        store.dispatch({ type: 'SHOW_CONFIRM_BOX', content, title, btnHuy, btnOK, titlebntHuy, titlebntOK })
    },
    hideConfirm: () => {
        store.dispatch({ type: 'HIDE_CONFIRM_BOX' })
    },
}

export default api;